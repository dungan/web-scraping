package vn.dung.igmor;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Application {
    public static void main(String[] args) throws IOException {

        List<Product> products = getAllProductLink()
                .map(Application::processProductLink)
                .filter(Product::isNotBlank)
                //.limit(3)
                //.forEach(System.out::println);
                .collect(toList());

        writeToExcelFile(products);
    }

    private static void writeToExcelFile(List<Product> products) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Product Name");
        header.createCell(1).setCellValue("Description");

        IntStream.range(0, products.size())
                .forEach(index -> {
                    Row row = sheet.createRow(index + 1);
                    row.createCell(0).setCellValue(products.get(index).getProductName());
                    row.createCell(1).setCellValue(products.get(index).getDescription());
                });

        FileOutputStream outputStream = FileUtils.openOutputStream(new File("Igmor Products.xlsx"));
        workbook.write(outputStream);
        outputStream.close();
    }

    private static Product processProductLink(String link) {
        System.out.println("Getting link: " + link);
        String name = "";
        String description = "";

        try {
            Document document = Jsoup.connect(link).timeout(60 * 1000).get();

            name = document
                    .select(".prod-info-wrap .pricebox .product-name-info span[itemprop=name]")
                    .text();

            description = document
                    .select(".prod-info-wrap .pricebox .short-desc-wrap .short-desc-txt span[itemprop=description]")
                    .text();

            String imageLink = document
                    .select(".prod-images-wrap .prod-photo a#zoomer")
                    .attr("abs:href");

            downloadImage(name, imageLink);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Product(name, description);
    }

    private static void downloadImage(String name, String imageLink) throws IOException {
        String finalName = name.replaceAll("[:\\\\/*\"?|<>']", "_");
        if (StringUtils.isEmpty(name)) {
            finalName = String.valueOf(System.currentTimeMillis());
        }
        finalName = "igmor-images/" + finalName + "." + FilenameUtils.getExtension(imageLink);

        FileOutputStream stream = FileUtils.openOutputStream(new File(finalName));

        stream.write(
                Jsoup.connect(imageLink)
                .timeout(60 * 1000)
                .ignoreContentType(true)
                .execute()
                .bodyAsBytes());

        stream.close();
    }

    private static Stream<String> getAllProductLink() throws IOException {
        return Jsoup.connect("http://www.igmor.com/showall.html")
                .get()
                .select("a[title*=Product].Medium_TXB_Bold")
                .stream()
                .map(element -> element.attr("abs:href"));
    }
}
