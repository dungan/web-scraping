package vn.dung.igmor;

import java.util.Objects;

public class Product {
    private String productName;
    private String description;

    public Product(String productName, String description) {
        this.productName = productName;
        this.description = description;
    }

    public boolean isBlank() {
        return Objects.equals(productName, "") && Objects.equals(description, "");
    }

    public boolean isNotBlank() {
        return !isBlank();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
