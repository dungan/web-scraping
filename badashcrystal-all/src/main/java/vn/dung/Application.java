package vn.dung;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Application {
    private static final String IN_STOCK = "In Stock";
    private static final String OUT_OF_STOCK = "Out Of Stock";
    private static final String OUTPUT_FILE_NAME = "BadashCrystal-all.xlsx";
    private static final int DEFAULT_TIMEOUT = 60 * 1000;

    private static final List<String> PRODUCT_URLS = Arrays.asList(
            "http://www.badashcrystal.com/collections",
            "http://www.badashcrystal.com/featured",
            "http://www.badashcrystal.com/barware",
            "http://www.badashcrystal.com/tableware",
            "http://www.badashcrystal.com/decor",
            "http://www.badashcrystal.com/gifts",
            "http://www.badashcrystal.com/awards-trophies"
    );

    public static void main(String[] args) throws IOException {
        List<Product> products = PRODUCT_URLS.stream()
                .flatMap(Application::extractProducts)
                .distinct()
                .peek(System.out::println)
                .collect(toList());

        writeToExcelFile(products);
    }

    private static void writeToExcelFile(List<Product> products) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("SKU");
        header.createCell(1).setCellValue("Price");
        header.createCell(2).setCellValue("Status");

        IntStream.range(0, products.size())
                .forEach(index -> {
                    Row row = sheet.createRow(index + 1);
                    row.createCell(0).setCellValue(products.get(index).getSku());
                    row.createCell(1).setCellValue(products.get(index).getPrice());
                    row.createCell(2).setCellValue(formatAvailable(products.get(index).isAvailable()));
                });

        FileOutputStream outputStream = FileUtils.openOutputStream(new File(OUTPUT_FILE_NAME));
        workbook.write(outputStream);
        outputStream.close();
    }

    private static String formatAvailable(boolean available) {
        if (available) {
            return IN_STOCK;
        }

        return OUT_OF_STOCK;
    }

    private static Stream<Product> extractProducts(String url) {
        try {
            Document document = Jsoup.connect(url).timeout(DEFAULT_TIMEOUT).get();
            Elements elements = document.select("ul.products-grid li.item");

            return elements.stream().map(Application::extractProduct);

        } catch (IOException e) {
            e.printStackTrace();
            return Stream.empty();
        }
    }

    private static Product extractProduct(Element element) {
        String sku = element.select("div.product-info div.price-box span.prod-sku").text().replace("\u00a0","").trim();
        String price = element.select("p.special-price span.price").text();
        if (StringUtils.isEmpty(price)) {
            price = element.select("div.price-box span.regular-price span.price").text();
        }
        boolean availability = IN_STOCK.equals(element.select("span.availability").text());

        return new Product(sku, price, availability);
    }
}
