package vn.dung;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

public class Application {

    private static final String ITEM_FILE = "Badash Item.xlsx";
    private static final String IN_STOCK = "In Stock";
    private static final String OUTPUT_FILE = IN_STOCK + "_" + ITEM_FILE;
    private static final String QUERY_URL = "http://www.badashcrystal.com/catalogsearch/result/?q=%s";

    public static void main(String[] args) throws IOException {

        List<String> inStockItems = getItemNoList(ITEM_FILE)
                .filter(Application::containInStock)
                .collect(toList());

        writeToOutputFile(inStockItems);
    }

    private static boolean containInStock(String itemNo) {
        try {
            System.out.println("Querying for item " + itemNo);

            Document document = getDocument(QUERY_URL, itemNo);
            Elements elements = document.select("ul.products-grid > li");
            if (elements.size() == 0) {
                return false;
            }

            for (Element element : elements) {
                if (isEqualsTrimHtml(element, ".prod-sku", itemNo) && isEquals(element, ".availability", IN_STOCK)) {
                    return true;
                }
            }

            return false;
        } catch (IOException e) {
            return false;
        }
    }

    private static boolean isEqualsTrimHtml(Element element, String selector, String str) {
        Elements select = element.select(selector);
        if (select.size() == 0) {
            return false;
        }

        String content = select.first().text().trim();
        if (content.length() < 2) {
            return false;
        }
        content = content.substring(0, content.length() - 2);

        return select.size() != 0 && String.valueOf(content).toLowerCase().equals(str.toLowerCase());
    }

    private static boolean isEquals(Element element, String selector, String str) {
        Elements select = element.select(selector);

        return select.size() != 0 && String.valueOf(select.first().text().trim()).toLowerCase().equals(str.toLowerCase());

    }

    private static void writeToOutputFile(List<String> inStockItems) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        sheet.createRow(0).createCell(0).setCellValue("ITEM NO");

        IntStream.range(0, inStockItems.size())
                .forEach(index ->
                        sheet.createRow(index + 1).createCell(0).setCellValue(inStockItems.get(index)));

        FileOutputStream outputStream = FileUtils.openOutputStream(new File(OUTPUT_FILE));
        workbook.write(outputStream);
        outputStream.close();
    }

    private static Stream<String> getItemNoList(String itemFile) throws IOException {
        InputStream inputStream = FileUtils.openInputStream(new File(itemFile));
        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheetAt(0);

        return StreamSupport.stream(sheet.spliterator(), false)
                .filter(row -> row.getRowNum() != 0)
                .map(row -> row.getCell(0))
                .map(Cell::getStringCellValue);
    }

    private static Document getDocument(String queryUrl, Object... params) throws IOException {
        return Jsoup.connect(String.format(queryUrl, params)).get();
    }
}
