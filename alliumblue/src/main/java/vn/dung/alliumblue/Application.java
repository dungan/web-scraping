package vn.dung.alliumblue;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Application {
    public static void main(String[] args) throws IOException {
        List<Item> items = getSubCategoriesLink()
                .filter(link -> StringUtils.startsWith(link, "http://www.alliumblue.com/"))
                //.filter(link -> link.contains("8908"))
                .flatMap(Application::convertToItem)
                .filter(Objects::nonNull)
                .collect(toList());

        writeToExcelFile(items);

//        getSubCategoriesLink()
//                .findFirst()
//                .map(Application::convertToItem)
//                .filter(Objects::nonNull)
//                .ifPresent(System.out::println);
    }

    private static void writeToExcelFile(List<Item> items) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Model");
        header.createCell(1).setCellValue("EAN");
        header.createCell(2).setCellValue("Price");

        IntStream.range(0, items.size())
                .forEach(index -> {
                    Row row = sheet.createRow(index + 1);
                    row.createCell(0).setCellValue(items.get(index).getModel());
                    row.createCell(1).setCellValue(items.get(index).getEan());
                    row.createCell(2).setCellValue("$" + items.get(index).getPrice() + "USD");
                });

        FileOutputStream outputStream = FileUtils.openOutputStream(new File("Alliumblue Items.xlsx"));
        workbook.write(outputStream);
        outputStream.close();
    }

    private static Stream<Item> convertToItem(String link) {
        try {
            System.out.println("Link: " + link);
            Document document = Jsoup.connect(link).timeout(120 * 1000).get();
            //TimeUnit.SECONDS.sleep(1);

            Elements itemsList = document.select("#productListing a[href^=http://www.alliumblue.com/].itemTitle");
            if (!itemsList.isEmpty()) {
                return itemsList
                        .stream()
                        .map(element -> element.attr("href"))
                        .flatMap(Application::convertToItem);
            }

            String model = document.select(".productinfoBox td[itemprop=model]").text();
            String price = document.select("font.productSalePrice span[itemprop=Price]").text();
            String ean = document.select("div.attribImg img[src^=/ean][src$=.png]").attr("src").replace("/ean", "").replace(".png", "");

            return Stream.of(new Item(model, ean, price));
        } catch (IOException | NullPointerException e) {
            System.out.println("Could not process this link: " + link);
            e.printStackTrace();
            return Stream.empty();
        }
    }

    private static Stream<String> getSubCategoriesLink() throws IOException {
        return Jsoup.connect("http://www.alliumblue.com/swarovski-strass-c-4176.html?zenid=f3serj86gscs3l6hc0vafgqvr4")
                .get()
                .select(".categoryListBoxContents .category_subcategories_list a")
                .stream()
                .map(element -> element.attr("href"));
    }
}
