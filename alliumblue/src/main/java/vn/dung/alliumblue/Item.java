package vn.dung.alliumblue;

public class Item {
    private String model;
    private String ean;
    private String price;

    public Item(String model, String ean, String price) {
        this.model = model;
        this.ean = ean;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Item{" + "model='" + model + '\'' +
                ", ean='" + ean + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
